/*
==================
PETERNAKAN STARDOO
==================
Peternakan Stardoo memiliki 3 jenis hewan: ayam, domba, dan sapi. Tiap jenis hewan bisa berjenis kelamin jantan maupun betina dan memiliki karakteristik yang berbeda-beda.
Karakteristik domba:
1. Domba jantan menghasilkan 0.1 kg wol setiap harinya.
2. Domba betina menghasilkan 0.15 kg wol setiap harinya.
Karakteristik sapi:
1. Sapi jantan beratnya naik 0.5 kg setiap harinya.
2. Sapi betina menghasilkan 20 liter susu setiap hari.
Karakteristik ayam:
1. Ayam jantan beratnya naik 0.2 kg setiap harinya.
2. Ayam betina bertelur setiap 2 hari sekali.

Tugas Anda adalah untuk membuat FUNGSI yang menerima tiga buah parameter, yaitu: `hewan`, `jenisKelamin`, dan `hari`.
Tampilkanlah hasil sesuai dengan karakteristik tiap-tiap hewan berdasarkan jenis kelamin dan berapa hari yang telah berlalu.

--------                                                                          
CONTOH 1                                                                            
--------                                                                           
hewan = 'Domba'                                                                     
jenisKelamin = 'betina'
hari = 2
OUTPUT:
Domba betina menghasilkan 0.3 kg wol setelah 2 hari

--------
CONTOH 2
--------
hewan = 'Sapi'
jenisKelamin = 'jantan'
hari = 4
OUTPUT:
Sapi jantan beratnya naik 2 kg setelah 4 hari

--------
CONTOH 3
--------
hewan = 'Sapi'
jenisKelamin = 'betina'
hari = 5
OUTPUT:
Sapi betina menghasilkan 100 liter susu setelah 5 hari

--------
CONTOH 4
--------
hewan = 'Ayam'
jenisKelamin = 'jantan'
hari = 5
OUTPUT:
Ayam jantan beratnya naik 1 kg setelah 5 hari

--------
CONTOH 5
--------
hewan = 'Ayam'
jenisKelamin = 'betina'
hari = 5
OUTPUT:
Ayam betina bertelur sebanyak 2 butir setelah 5 hari
RULE:
- Hanya boleh menggunakan built in function Math
*/

// isi variabel yang tersedia boleh diubah-ubah sesuai kebutuhan

let hewan = 'Ayam';
let jenisKelamin = 'betina';
let hari = 7;

// Tulis code di sini...
const peternakan = (hewan, jenisKelamin, hari) => {
    switch (hewan) {
        case "Domba":
            switch (jenisKelamin) {
                case "jantan":
                    return `Domba jantan menghasilkan ${0.1 * hari} kg wol setelah ${hari} hari.`;
                case "betina":
                    return `Domba betina menghasilkan ${0.15 * hari} kg wol setelah ${hari} hari.`;
                default:
                    return "invalid input jenis kelamin"
            }
        case "Sapi":
            switch (jenisKelamin) {
                case "jantan":
                    return `Sapi jantan beratnya naik ${0.5 * hari} kg setelah ${hari} hari.`;
                case "betina":
                    return `Sapi betina menghasilkan ${20 * hari} liter susu setelah ${hari} hari.`;
                default:
                    return "invalid input jenis kelamin"
            }
        case "Ayam":
            switch (jenisKelamin) {
                case "jantan":
                    return `Ayam jantan beratnya naik ${0.2 * hari} kg setelah ${hari} hari.`;
                case "betina":
                    return `Ayam betina bertelur sebanyak ${Math.floor(hari/2)} butir setelah ${hari} hari.`;
                default:
                    return "invalid input jenis kelamin"
            }
        default:
            return "Invalid input";
    }
};

console.log(peternakan(hewan, jenisKelamin, hari));