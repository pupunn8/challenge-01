/**
  ============
  Capital case
  ============
 
  Sebuah fungsi bernama capitalCase akan menerima satu buah parameter yang berupa array of string,
  Fungsi ini akan mengembalikan array of string yang dimana setiap huruf pertama pada element nya
  akan berubah menjadi huruf besar
 
  Example:
  @input -> ['semmi', 'verian', 'putera']
  @output -> ['Semmi', 'Verian', 'Putera']
 
  RULES :
  - Kerjakan tanpa built-in function kecuali push(), Number(), String(), toString(), toLowerCase(), toUpperCase() 
 */

 function capitalCase(arr) {
     let result = [];
     for(value of arr) {
        let res = "";
        for (let i = 0; i < value.length; i++) {
            if(i === 0) {
                res += value[i].toUpperCase();
            } else {
                res += value[i];
            }
        }
        result.push(res);
    }
    return result;
}

console.log(capitalCase(['semmi', 'verian', 'putera'])) // ['Semmi', 'Verian', 'Putera']
console.log(capitalCase(['Naufal', 'muhaMmad', 'siddiq'])) // ['Naufal', 'MuhaMmad', 'Siddiq']
