/**
  ==========================
  What is my email provider?
  ==========================
 
  Description
  -----------
  Email merupakan sebuah cara untuk kita berinteraksi antar satu dengan yang lainnya secara elektronik,
  Banyak sekali provider yang menyediakan layanan email ini.
 
  Instruction
  -----------
  Buatlah sebuah FUNGSI yang akan mengeluarkan output provider email yang digunakan oleh user.
 
  Contoh
  =======
  @input => lingga@gmail.com
  @output => Your email provider is gmail
  (tidak menggunakan .com di belakang)
  
  @input => angga@kampusmerdeka.gov.id
  @output => Your email provider is kampusmerdeka
  (tidak menggunakan .gov.id di belakang)
 
  Rules
  =====
  1. Tidak diperbolehkan menggunakan built-in function:
     .map .filter .reduce .split .join .indexOf .findIndex .substring
 
 */

// your code here
// let email = "angga@kampusmerdeka.gov.id";
let email = "pupunn@students.unnes.ac.id";

const emailProvider = email => {
    let res = "";
    let c = 0;
    for (let i = 0; i < email.length; i++) {
        if(email[i] === "@") {
            for(let j = i + 1; j < email.length; j++) {
                res += email[j];
                
                if(email[j + 1] === ".") {
                    c++;
                    if(c = 2) {
                        console.log(c);
                        break;
                    }
                }

            }
        }
    }
    return res;
};

console.log(emailProvider(email));