/*  Dibawah ini merupakan fungsi yang bernama 'printStatement()'.
    Anda diharapkan untuk melengkapi fungsi tersebut agar ketika fungsi dijalankan hasilnya menjadi:
    
=>  Halo teman-teman Binar Academy,
=>  perkenalkan nama saya {nama kalian},
=>  saya adalah seorang "Back End Developer".

    Struktur hasil string harus sesuai hasil diatas.
*/

function printStatement() {
    // Tulis code kalian dibawah ini (code dapat kalian tambah/ubah sesuai kreativitas kalian).
    const nama      = "Arif Saifunnasrullah";
    const hasil     = `
    Halo teman-teman Binar Academy,
    perkenalkan nama saya ${nama},
    saya adalah seorang "Back End Developer"
    `;
    console.log(hasil);

}

printStatement();