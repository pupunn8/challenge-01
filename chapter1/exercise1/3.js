/*  Dibawah ini merupakan fungsi yang bernama 'printWithArr()' 
    Anda diharapkan untuk melengkapi fungsi tersebut agar ketika fungsi dijalankan hasilnya menjadi:
    
     OUTPUT:
=>   Hari ini saya pergi ke {Seaworld} bersama teman-teman.
=>   Disana saya membeli melihat ikan {Lumba-lumba}, {Hiu}, dan {Pari}.
=>   Saat itu rombongan kami berjumlah {3} orang, dengan harga tiket per orang sebesar Rp{75000}, sehingga total harga menjadi {225000}.
=>   Kami pulang menggunakan {Ojek Online}. 

    Simbol kurawal diantara kata wajib menggunakan DATA dari ARRAY yang diberikan. Struktur hasil string harus sesuai hasil diatas.
*/

function printWithArr() {
    const arr = [
        ['Lumba-lumba', 'Kura-kura', 'Hiu', 'Udang', 'Pari'], 
        ['Dufan', 'Atlantis', 'Seaworld'],
        [
            {
                namaPengunjung: 'Jan',
                usia: 20,
                orangDewasa: true,
            },
            {
                namaPengunjung: 'Ken',
                usia: 19,
                orangDewasa: true,
            },
            {
                namaPengunjung: 'Pon',
                usia: 15,
                orangDewasa: false,
            },
        ],
        {
            hargaTiket: 75000,
            jumlahPengunjung: 3,
        },
        'Ojek Online'
    ]
    // Tulis code kalian dibawah ini (code dapat kalian tambah/ubah sesuai kreativitas kalian).
    const hewan         = arr[0];
    const lokasi        = arr[1];
    const pengunjung    = arr[2];
    const tiket         = arr[3];
    const transportasi  = arr[4];

    const hasil = `
    Hari ini saya pergi ke ${lokasi[2]} bersama teman-teman.
    Disana saya melihat ikan ${hewan[0]}, ${hewan[2]}, dan ${hewan[4]}.
    Saat itu rombongan kami berjumlah ${pengunjung.length} orang, dengan harga tiket per orang sebesar Rp${tiket.hargaTiket}, sehingga total harga menjadi ${tiket.hargaTiket * tiket.jumlahPengunjung}.
    Kami pulang menggunakan ${transportasi}. 
    `;
    console.log(hasil);
}

printWithArr();