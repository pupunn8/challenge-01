/*  Dibawah ini merupakan fungsi yang bernama 'printWithObj()'.
    Anda diharapkan untuk melengkapi fungsi tersebut agar ketika fungsi dijalankan hasilnya menjadi:
    
     OUTPUT:
=>   Halo nama saya {Gary Neville}, hari ini saya pergi ke {Gramevia}.
=>   Disana saya membeli {Buku Tulis} sebanyak {2} buah, Buku Coding sebanyak {5} buah, dan Mocking Bird sebanyak {2} buah.
=>   Selain itu, saya juga menemukan beberapa alat tulis seperti {Pulpen}, {Pensil}, dan {Penghapus}.
=>   Total belanja saya hari ini sebesar: {117000}.

    Simbol kurawal diantara kata wajib menggunakan DATA dari OBJECT yang diberikan. Struktur hasil string harus sesuai hasil diatas.
*/

function printWithObj() {
    const objPerjalanan = {
        objNama: {
            nama1: 'Putri Amelia',
            nama2: 'Clinton Haver',
            nama3: 'Gary Neville',
        },
        arrAlatTulis: ['Pulpen', 'Penggaris', 'Pensil', 'Cermin', 'Spidol', 'Penghapus', 'Papan Tulis'],
        arrInfoBarang: [
            {
                namaBarang: 'Buku Tulis',
                harga: 5000,
                kuantitas: 2,  
            },
            {
                namaBarang: 'Buku Coding',
                harga: 20000,
                kuantitas: 5,
            },
            {
                namaBarang: 'Harry Potter',
                harga: 7500,
                kuantitas: 1,  
            },
            {
                namaBarang: 'Majalah',
                harga: 2500,
                kuantitas: 10,  
            },
            {
                namaBarang: 'Buku Masak',
                harga: 3000,
                kuantitas: 20,  
            },
            {
                namaBarang: 'Mocking Bird',
                harga: 3500,
                kuantitas: 2, 
            }
        ],
        namaToko: 'Gramevia',
    }
    // Tulis code kalian dibawah ini (code dapat kalian tambah/ubah sesuai kreativitas kalian).
    const nama          = objPerjalanan.objNama;
    const alatTulis     = objPerjalanan.arrAlatTulis;
    const infoBarang    = objPerjalanan.arrInfoBarang;
    const toko          = objPerjalanan.namaToko;
    const totalBelanja  = (infoBarang[0].kuantitas * infoBarang[0].harga) + (infoBarang[1].kuantitas * infoBarang[1].harga) + (infoBarang[5].kuantitas * infoBarang[5].harga);
    const res = `
    Halo nama saya ${nama.nama3}, hari ini saya pergi ke ${toko}.
    Disana saya membeli ${infoBarang[0].namaBarang} sebanyak ${infoBarang[0].kuantitas} buah, ${infoBarang[1].namaBarang} sebanyak ${infoBarang[1].kuantitas} buah, dan ${infoBarang[5].namaBarang} sebanyak ${infoBarang[5].kuantitas} buah.
    Selain itu, saya juga menemukan beberapa alat tulis seperti ${alatTulis[0]}, ${alatTulis[2]}, dan ${alatTulis[5]}.
    Total belanja saya hari ini sebesar: ${totalBelanja}.
    `; 
    console.log(res);
}

printWithObj();