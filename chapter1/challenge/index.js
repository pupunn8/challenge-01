const readline = require('readline');
let lineNumber = 0;
let operan;
let x, y;

process.stdin.setEncoding('utf8');
const rl = readline.createInterface({
  input: process.stdin,
  terminal: false
});

rl.on('line', readLine);
console.log("Pilih angka sesuai operasi yang ingin dilakukan:");
console.log(`
1. Tambah
2. Kurang
3. Kali
4. Bagi
5. Akar Kuadrat
6. Luas Persegi
7. Volume Kubus
8. Volume Tabung
`);

function readLine (line) {
    if (lineNumber == 0) {
        if (!isNaN(parseInt(line)) && parseInt(line) < 9 && parseInt(line) > 0) {
            operan = parseInt(line);
            lineNumber++;
            console.log("Anda memilih operasi " + cekOperasi(operan));
            console.log("Masukkan input pertama: ");
        } else {
            console.log("Invalid Input");
        }
    } else if (lineNumber == 1) {
        if (!isNaN(parseInt(line)) && (operan < 5 || operan == 8)) {
            x = parseInt(line);
            lineNumber++;
            console.log(`Input Pertama = ${x}`);
            console.log("Masukkan input kedua: ");
        } else if (!isNaN(parseInt(line)) && operan < 8) {
            x = parseInt(line);
            let result = operasi(operan, x);
            console.log("Hasil: " + result);
            process.exit();
        } else{
            console.log("Invalid Input");
        }
    } else {
        if (!isNaN(parseInt(line))) {
            y = parseInt(line);
            let result = operasi(operan, x, y);
            console.log(`Input Kedua = ${y}`);
            console.log("Hasil: " + result);
            process.exit();
        } else {
            console.log("Invalid Input");
        }
    }
}

const cekOperasi = (x) => {
    switch (x) {
        case 1:
            return "1. Tambah (x + y)";
        case 2:
            return "2. Kurang (x - y)";
        case 3:
            return "3. Kali (x * y)";
        case 4:
            return "4. Bagi (x / y)";
        case 5:
            return "5. Akar Kuadrat (x Akar Kuadrat)";
        case 6:
            return "6. Luas Persegi (s x s)";
        case 7:
            return "7. Volume Kubus (s x s x s)";
        case 8:
            return "8. Volume Tabung (22/7 x r x r x t)";
        default:
            break;
    }
}

const operasi = (a, x, y) => {
    switch (a) {
        case 1:
            return x + y;
        case 2:
            return x - y;
        case 3:
            return x * y;
        case 4:
            return x / y;
        case 5:
            return Math.sqrt(x);
        case 6:
            return x ** 2;
        case 7:
            return x ** 3;
        case 8:
            return 22/7 * x * x * y;
        default:
            break;
    }
}