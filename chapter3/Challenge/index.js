const { Client } = require("pg");

const client = new Client({
    user: 'postgres',
    port: '5432',
    host: 'localhost',
    password: '1',
    database: 'yugioh_game' //step2
});

const authenticated = (email, password) => {
    return ("arif@mail.com" === email) && ("12121212" === password);
};

const runQuery = (query) => {
    if(authenticated) {
        client.connect();
        client.query(query, (err, res) => {
            if(err) {
                console.log("Query Gagal!");
            } else {
                console.log("Query berhasil!");
                if(res.rows.length !== 0) console.log(res.rows);
            };
            client.end();
        });
    } else {
        return console.log("Anda tidak berhak mengakses database");
    }
};

const queries = {
    createDatabase : `CREATE DATABASE yugioh_game`,
    createUserGameTable : `CREATE TABLE user_games (
        game_id serial,
        email VARCHAR(100) NOT NULL,
        password VARCHAR(100) NOT NULL,
        PRIMARY KEY (game_id)
      );`,
    createUserBiodatasTable : `CREATE TABLE user_game_biodatas (
        biodata_id serial,
        game_id int NOT NULL,
        player_name VARCHAR(100) NOT NULL,
        level int NOT NULL,
        PRIMARY KEY (biodata_id),
        CONSTRAINT fk_game_id FOREIGN KEY (game_id) REFERENCES user_games(game_id)
      );`,
    createUserHistoriesTable : `CREATE TABLE user_game_histories (
        history_id serial,
        game_id int NOT NULL,
        is_win boolean NOT NULL,
        enemy_id int NOT NULL,
        total_turn int NOT NULL,
        play_date timestamp NOT NULL,
        PRIMARY KEY (history_id),
        FOREIGN KEY (game_id) REFERENCES user_games(game_id) ON DELETE CASCADE
      );`,
      insertUserGameTable : `INSERT INTO user_games 
            (email, password)
        VALUES
            ('a@mail.com','12121210'),
            ('b@mail.com','12121211'),
            ('c@mail.com','12121212'),
            ('d@mail.com','12121213'),
            ('e@mail.com','12121214');
      `,
      insertUserBiodatasTable : `INSERT INTO user_game_biodatas 
            (game_id, player_name, level)
        VALUES
            (1, 'aname', 23),
            (2, 'bname', 15),
            (3, 'cname', 42),
            (4, 'dname', 13),
            (5, 'ename', 33);
      `,
      insertUserHistoriesTable : `INSERT INTO user_game_histories 
            (game_id, is_win, enemy_id, total_turn, play_date)
        VALUES
            (1, '1', 4, 24, '2022-03-25 12:20:32'),
            (1, '0', 5, 2, '2022-03-26 12:30:21'),
            (1, '1', 3, 8, '2022-03-26 08:13:59'),
            (2, '1', 1, 12, '2022-03-25 23:20:26'),
            (2, '1', 5, 20, '2022-03-27 16:26:25'),
            (3, '0', 4, 13, '2022-03-28 21:21:21');
      `,
      readData : `SELECT * FROM user_games 
      JOIN user_game_biodatas 
      ON user_games.game_id = user_game_biodatas.game_id
      LEFT JOIN user_game_histories 
      ON user_games.game_id = user_game_histories.game_id
      `,
      updateData : `UPDATE user_game_biodatas
        SET player_name = 'nama_baru'
        WHERE game_id = 1
      `,
      dummyData : `INSERT INTO user_games 
            (email, password)
        VALUES
            ('aku.dummy.data@mail.com','hapusaku');
      `,
      deleteData : `DELETE FROM user_games
        WHERE email = 'aku.dummy.data@mail.com';
      `
};

authenticated("arif@mail.com", 12121212);
// runQuery(queries.createDatabase); // Step 1
// runQuery(queries.createUserGameTable); // Step 2 dan aktifkan db name
// runQuery(queries.createUserBiodatasTable); // Step 3
// runQuery(queries.createUserHistoriesTable); // Step 4
// runQuery(queries.insertUserGameTable); // Step 5
// runQuery(queries.insertUserBiodatasTable); // Step 6
// runQuery(queries.insertUserHistoriesTable); // Step 7
// runQuery(queries.readData); // Step 8
// runQuery(queries.updateData); // Step 9
// runQuery(queries.dummyData); // Step 10
runQuery(queries.deleteData); // Step 11