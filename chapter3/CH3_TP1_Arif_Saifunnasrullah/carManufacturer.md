# `ERD Exercises - Car Manufacturer`

## Directions

- Sebuah perusahaan mobil menghasilkan berbagai mobil. `Cars` memiliki beberapa informasi seperti: `car_id`, `car_name`, `price` dan attribut lainnya yang berkaitan dengan relasi.
    - `Cars` dibuat dari berbagai `Spareparts`.
    - `Cars` memiliki `Models`.
    - `Cars` memiliki `Customers`.
- `Spareparts` memiliki beberapa informasi seperti: `sparepart_id`, `sparepart_name`, `stock` dan attribut lainnya yang berkaitan dengan relasi.
    - `Spareparts` dapat membuat banyak `Cars`.   

- `Models` memiliki beberapa informasi seperti: `model_id`, `model_name`, `model_code` dan attribut lainnya yang berkaitan dengan relasi.
    - `Models` memiliki banyak `Cars`.
- `Customers` memiliki beberapa informasi seperti: `customer_id`, `customer_name`, `address`, `phone_number` dan attribut lainnya yang berkaitan dengan relasi.

### Objectives

- Buatlah ERD untuk menggambarkan poin2 [Directions](#directions) yang disebutkan diatas.
- Tunjukkan mana yang merupakan `entitas`, `primary key`, `foreign key`, `attributes` untuk setiap entitas, dan `relasi` diantara entitas.
- Bisa jadi terdapat `entitas` tambahan yang menggambarkan kriteria-kriteria di atas. Tambahkan sesuai analisis Anda. 
