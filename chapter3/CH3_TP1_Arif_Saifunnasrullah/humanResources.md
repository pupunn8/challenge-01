# `ERD Exercises - Human Resources`

## Directions

- Diketahui bahwa suatu perusahaan HR menginginkan pemetaan database untuk memetakan karyawan.
- Pertama, terdapat `Employees` yang memiliki informasi sebagai berikut: `employee_id`, `name`, dan attribut lainnya yang berkaitan dengan relasi.
    - Setiap `Employees` memiliki satu `Profiles` dan memiliki satu `Positions`.
- Kedua, terdapat `Profiles` yang memiliki informasi sebagai berikut: `profile_id`, `gender`, `age`, `marital_status`, dan attribut lainnya yang berkaitan dengan relasi.
    - Setiap `Profiles` memiliki satu `Addresses`.
- Ketiga, terdapat `Positions` yang memiliki informasi sebagai berikut: `position_id`, `name`, dan attribut lainnya yang berkaitan dengan relasi.
    - `Positions` memiliki banyak `Employees`.
- Keempat, `Addresses` memiliki informasi sebagai berikut: `address_id`, `address_description`.


### Objectives

- Buatlah ERD untuk menggambarkan poin2 [Directions](#directions) yang disebutkan diatas.
- Tunjukkan mana yang merupakan `entitas`, `primary key`, `foreign key`, `attributes` untuk setiap entitas, dan `relasi` diantara entitas. 