# `ERD Exercises - Kalibrr Wannabe`

## Directions

- Diketahui terdapat suatu perusahaan bernama `Kalibrr Wannabe` yang menginginkan pemetaan database terhadap aplikasi talent hiring. 
- `Careers` memiliki informasi sebagai berikut: `career_id`, `name`, `description`, `requirement` dan attribut lainnya yang berkaitan dengan relasi.
    - Setiap `Careers` memiliki `Positions`.
    - `Careers` memiliki banyak `Applications`.
- `Departments` memiliki informasi sebagai berikut: `dept_id`, `name`, dan attribut lainnya yang berkaitan dengan relasi.
    - `Departments` memiliki banyak `Positions`.
- `Positions` memiliki informasi sebagai berikut: `position_id`, `name`, dan attribut lainnya yang berkaitan dengan relasi.
    - `Positions` memiliki banyak `Careers`.
- `Applications` memiliki informasi sebagai berikut: `apply_id`, `name`, `email`, `message`, `resume`, dan attribut lainnya yang berkaitan dengan relasi.
    - Setiap `Applications` memiliki `Careers`.

### Objectives

- Buatlah ERD untuk menggambarkan poin2 [Directions](#directions) yang disebutkan diatas.
- Tunjukkan mana yang merupakan `entitas`, `primary key`, `foreign key`, `attributes` untuk setiap entitas, dan `relasi` diantara entitas. 