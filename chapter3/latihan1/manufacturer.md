# `ERD Exercises - Manufacturer`

## Directions

- Sebuah perusahaan manufaktur menghasilkan berbagai produk. `Produk` yang dihasilkan memiliki beberapa informasi seperti: `product_name`, `product_id`, dan `quantity`.
- `Produk` dibuat dari berbagai `Komponen`. Pikirkan baik-baik bagaimana menjembatani relasi ini.
- Setiap `Komponen` dapat disupply oleh satu atau lebih `Supplier`. Pikirkan baik-baik bagaimana menjembatani relasi ini.
- Informasi untuk `Komponen` yaitu sebagai berikut: `component_id`, `component_name`, `description`, `suppliers yang memberikan supply`, dan `produk yang menggunakan komponen`.

### Objectives

- Buatlah ERD untuk menggambarkan poin2 [Directions](#directions) yang disebutkan diatas.
- Tunjukkan mana yang merupakan `entitas`, `primary key`, `foreign key`, `attributes` untuk setiap entitas, dan `relasi` diantara entitas.
- Bisa jadi terdapat `entitas` tambahan yang menggambarkan kriteria-kriteria di atas. Tambahkan sesuai analisis Anda. 
