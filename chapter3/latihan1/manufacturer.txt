Entitas     : Product
Primary Key : product_id
Foreign Key : -
Attributes  : product_name, quantity
Relasi      : one-to-many ke Product_Component dan many-to-many ke Component
##--##
Entitas     : Component
Primary Key : component_id
Foreign Key : -
Attributes  : component_name, description
Relasi      : one-to-many ke Product_Component, Component_Supplier dan many-to-many ke Product, Supplier
##--##
Entitas     : Product_Component
Primary Key : product_component_id
Foreign Key : product_id, component_id
Attributes  : -
Relasi      : one-to-one ke Product dan Component
##--##
Entitas     : Supplier
Primary Key : supplier_id
Foreign Key : -
Attributes  : supplier_name, address
Relasi      : one-to-many ke Component_Supplier dan many-to-many ke Component
##--##
Entitas     : Component_Supplier
Primary Key : component_supplier_id
Foreign Key : component_id, supplier_id
Attributes  : -
Relasi      : one-to-one ke Component dan Supplier
##--##