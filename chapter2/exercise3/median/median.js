function cariMedian(arr) {
    let res = 0;
    let median = arr.length % 2 == 0 ? 1 : 0;
    if (median) {
        let nilai_tengah = (arr.length / 2) - 1
        res = (arr[nilai_tengah] + arr[nilai_tengah + 1]) / 2;
    } else {
        let nilai_tengah = (arr.length + 1) / 2;
        res = arr[nilai_tengah - 1];
    }
    return res;
}

// TEST CASES
console.log(cariMedian([1, 2, 3, 4, 5])); // 3
console.log(cariMedian([1, 3, 4, 10, 12, 13])); // 7
console.log(cariMedian([3, 4, 7, 6, 10])); //7
console.log(cariMedian([1, 3, 3])); // 3
console.log(cariMedian([7, 7, 8, 8])); // 7.5