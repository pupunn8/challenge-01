let vocal = "aiueoAIUEO";
function vocalSeeker(board) {
    let jumlahVokal = 0;
    let vokal = "";
    for (let i = 0; i < board.length; i++) {
        for (let j = 0; j < board[i].length; j++) {
            for (let k = 0; k < vocal.length; k++) {
                if(board[i][j] === vocal[k]) {
                    jumlahVokal++;
                    vokal += board[i][j];
                }
            }
        }
    }
    return `Vokal ditemukan ${jumlahVokal} dan kumpulan vokal adalah ${vokal}`;
}

//DRIVER CODE

let board = [
    ['*', '*', '*', 10],
    ['*', '*', -5, -10, '*', 100],
    ['a', 'A', 'o', 'b'],
]
console.log(vocalSeeker(board)); // vokal ditemukan 3 dan kumpulan vokal adalah aAo