function ladder(word) {
    let res     = [];
    let arr     = [];
    let counter = word.length - 1;
    while (counter >= 0) {
        for (let i = 0; i <= counter; i++) {
            arr.push(word[i]);
        }
        res.push(arr);
        arr = [];
        counter--;
    }
    return res;
}
  
  // DRIVER CODE
  console.log(ladder('4angrymen'));