const readline = require('readline');
lineNumber = 0;

process.stdin.setEncoding('utf8');
const rl = readline.createInterface({
    input: process.stdin,
    terminal: false
});

//kumpulan fungsi
const nilaiMax = x => Math.max(...x);
const nilaiMin = x => Math.min(...x);
const nilaiAvg = x => Math.round((x.reduce((a, b) => a + b, 0)) / x.length);
const cekLulus = x => x.filter(x => x >= 75).length;
const bubbleSort  = x => {
    for (let i = x.length; i >= 0; i--) {
        for (let j = 1; j <= i; j++) {
            if(x[j-1] > x[j]) {
                let temp = x[j - 1];
                x[j-1] = x[j];
                x[j] = temp;
            }
        }
    }
    return x;
}

console.log("Masukkan nilai dan ketik \"q\" jika sudah selesai :");

let lineno = 0;
nilai = [];
rl.on('line', function (line) {
    lineno++;
    // pengecekan nilai
    if (!isNaN(parseInt(line)) && parseInt(line) <= 100 && parseInt(line) >= 0) {
        nilai[nilai.length] = parseInt(line);
    } else if (line == "q") {
        // tampilkan hasil
        console.log(
            `
Didapatkan hasil rekap belajar siswa sebagai berikut :
Nilai tertinggi siswa : ${nilaiMax(nilai)}
Nilai terendah siswa : ${nilaiMin(nilai)} 
Nilai rata-rata siswa : ${nilaiAvg(nilai)}
Jumlah siswa yang lulus : ${cekLulus(nilai)}
Jumlah siswa yang tidak lulus : ${nilai.length - cekLulus(nilai)}
Urutan nilai dari yang terendah hingga tertinggi : ${bubbleSort(nilai)}
            `
        );
        process.exit();
    } else {
        console.log("Invalid Input");
    }

});
