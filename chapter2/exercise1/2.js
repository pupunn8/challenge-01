/*  Buatlah pseudocode untuk kasus berikut: Seorang pengajar sedang memeriksa ujian mahasiswa 
    dan akan memberikan desc nilai dari A-E dengan ketentuan sebagai berikut:

    Nilai 80 - 100: A
    Nilai 65 - 79: B
    Nilai 50 - 64: C
    Nilai 35 - 49: D
    Nilai 0 - 34: E

    Tampilkan desc nilai dan nama siswa saat pengajar tersebut memasukkan nilai dan nama yang dia inginkan.

    output yang diharapkan => nama: Andhika; score: A

*/

/* 
PSEUDOCODE
SET nama WITH "anyString"
SET nilai WITH "anyNumber"

IF (nilai less than or equal 100) AND (nilai greater than or equal 80) THEN
    DISPLAY nama : "nama"; score : A
ELSE IF (nilai less than or equal 79) AND (nilai greater than or equal 65) THEN
    DISPLAY nama : "nama"; score : B
ELSE IF (nilai less than or equal 64) AND (nilai greater than or equal 50) THEN
    DISPLAY nama : "nama"; score : C
ELSE IF (nilai less than or equal 49) AND (nilai greater than or equal 35) THEN
    DISPLAY nama : "nama"; score : D
ELSE IF (nilai less than or equal 34) AND (nilai greater than or equal 0) THEN
    DISPLAY nama : "nama"; score : E
ELSE
    DISPLAY Invalid input
ENDIF

*/