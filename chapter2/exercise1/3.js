/* Diberikan sebuah variable kata bertipe String.

Buatlah pseudocode dimana program tersebut akan menghitung jumlah karakter x dan jumlah karakter o. 
Setelah perhitungan selesai maka tampilkan true jika jumlah karakter o dan x sama dan false jika tidak

Contoh 1:
let kata = 'xoxoxo'

maka output adalah true

          
Contoh 2:
let kata = 'oxooxox'

maka output adalah false
*/

/*
PSEUDOCODE
SET kata WITH "anyString"
SET x WITH 0
SET o WITH 0

FOR each character in "kata"
    IF (character is equal to x) THEN
        INCREMENT x
    ELSE IF (character is equal to o) THEN
        INCREMENT o
    ELSE
        DISPLAY Invalid input
        ENDFOR
    ENDIF
ENDFOR

IF ("x" is equal to "o") THEN
    DISPLAY true
ELSE
    DISPLAY false
ENDIF
*/