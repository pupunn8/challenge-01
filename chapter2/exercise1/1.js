/**
 * =============
 * Disney Island
 * =============
 *
 * Buatlah pseudocode untuk kasus berikut:
 * 
 * Sebuah wahana bermain 'Disney Island' akan memberikan tarif sesuai dengan tinggi dan umur anak
 * dengan ketentuan sebagai berikut:
 * - Umur 1 tahun ke bawah: Tampilkan 'Dilarang masuk'
 * - Umur 2-3 tahun: Rp 30.000. Jika tinggi anak umur 2-3 tahun lebih dari 70cm maka tarif akan bertambah 10.000
 * - Umur 4-7 tahun: Rp 40.000. Jika tinggi anak umur 4-7 tahun lebih dari 120cm maka tarif akan bertambah 15.000
 * - Umur 8-10 tahun: Rp 50.000. Jika tinggi anak umur 8-10 tahun lebih dari 150cm maka tarif akan bertambah 20.000
 * - Umur diatas 10 tahun : Rp 80.000
 * Tampilkan tarif harga sesuai umur dan tinggi seorang anak!
 *
**/

/*
PSEUDOCODE
SET umur WITH "anyNumber"
SET tinggi WITH "anyNumber"
SET tarif WITH 0

IF (umur less than 1) THEN
    DISPLAY Dilarang Masuk
ELSE IF (umur less than or equal 3) AND (umur greater than or equal 2) THEN
    tarif = 30000
    IF (tinggi greater than 70) THEN
        tarif + 10000
    ENDIF
ELSE IF (umur less than or equal 7) AND (umur greater than or equal 4) THEN
    tarif = 40000
    IF (tinggi greater than 120) THEN
        tarif + 15000
    ENDIF
ELSE IF (umur less than or equal 10) AND (umur greater than or equal 8) THEN
    tarif = 50000
    IF (tinggi greater than 150) THEN
        tarif + 20000
    ENDIF
ELSE IF (umur greater than 10) THEN
    tarif = 80000
ENDIF

DISPLAY "tarif"
*/