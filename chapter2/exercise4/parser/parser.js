class Parser {
    static parseIncomingArray(arr) {
        let res = "";
        for (let i = 0; i < arr.length; i++) {
            for (let j = 0; j < arr[i].length; j++) {
                res += arr[i][j];
                if(!(i === arr.length - 1 && j === arr[i].length - 1)) {
                    res += ", ";
                }
            }
        }
        return res;
    }

    static parseIncomingObject(obj) {
        let res = "";
        res += obj.name + ", ";
        res += obj.age + ", ";
        for (let i = 0; i < obj.firstArray.length; i++) {
            res += obj.firstArray[i] + ", ";
        }
        res += obj.firstObject.name + ", ";
        res += obj.firstObject.age;
        return res;
    }

    static parseIncomingUrl(url){
        const res = {};
        res.protocol    = url.split(":")[0];
        let oriUrl      = url.split("//")[1];
        res.originalUrl = oriUrl.split("?")[0];
        res.query       = oriUrl.split("?")[1];
        let params      = res.query.split("&");
        let obj = {};
        for (let i = 0; i < params.length; i++) {
            let kata1 = params[i].split("=")[0];
            let kata2 = params[i].split("=")[1];
            obj[kata1] = kata2;
        }
        res.params = obj;
        return res;
    };
}

console.log(Parser.parseIncomingArray([[2, 4, 6], [5, 7, 8], [1], [10, 'hello']])); // 2, 4, 6, 5, 7, 8, 1, 10, hello
console.log(Parser.parseIncomingObject({name: 'Arlingga', age: 20, firstArray: [1, 2, 3], firstObject: {name : 'Steven', age: 20}})); // Arlingga, 20, 1, 2, 3, Steven, 20

console.log(Parser.parseIncomingUrl('https://github.com/pubnub/python/search?utf8=%E2%9C%93&q=python')); 
// {
//   protocol: 'https',
//   originalUrl: 'github.com/pubnub/python/search',
//   query: 'utf8=%E2%9C%93&q=python',
//   params: { utf8: '%E2%9C%93', q: 'python' }
// }