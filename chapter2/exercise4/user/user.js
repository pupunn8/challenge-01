class User {
    constructor(props) {
        let {name, email, password} = props;
        this.name = name;
        this.email = email;
        this.password = this.#passwordGenerator(password);
    }

    #changeChar(arr1, arr2, str) {
        let res = "";
        for(let i = 0; i < str.length; i++) {
            let temp        = "";
            for (let j = 0; j < arr1.length; j++) {
                if(str[i] === arr1[j]) {
                    temp = arr2[j];
                } else if(str[i] === arr2[j] && arr1.length == 26) {
                    temp = arr1[j];
                }
            }
            
            if(temp) {
                res += temp;
            } else {
                res += str[i];
            }
        }
    
        return res;
    };

    #changeVocals(str) {
        // const arrKata1  = ["A", "E", "I", "U", "O", "a", "e", "i", "u", "o"];
        // const arrKata2  = ["B", "F", "J", "V", "P", "b", "f", "j", "v", "p"];
        const charCode  = [1, 5, 9, 15, 21];
        const arrKata1  = [];
        const arrKata2  = [];
    
        for (let i = 0; i < charCode.length; i++) {
            arrKata1[arrKata1.length] = String.fromCharCode(charCode[i] + 64);
            arrKata1[arrKata1.length] = String.fromCharCode(charCode[i] + 96);
            arrKata2[arrKata2.length] = String.fromCharCode(charCode[i] + 65);
            arrKata2[arrKata2.length] = String.fromCharCode(charCode[i] + 97);
        }
    
        return this.#changeChar(arrKata1, arrKata2, str);
    }

    #reverseWord(str) {
        let res = "";
        for (let i = str.length - 1; i >= 0; i--) {
            res += str[i];
        }
    
        return this.#changeVocals(res);
    }

    #setLowerUpperCase(str) {
        const kecil     = [];
        const besar     = [];
        const length    = 26;
    
        for (let i = 0; i < length; i++) {
            kecil[kecil.length] = String.fromCharCode(i + 65);
            besar[besar.length] = String.fromCharCode(i + 97);
        }
    
        let res = this.#changeChar(kecil, besar, str);
    
        return this.#reverseWord(res);
    }

    #removeSpaces(str) {
        let res = "";
        for (let i = 0; i < str.length; i++) {
            if (str[i] !== " ") {
                res += str[i];
            }
        }
        return this.#setLowerUpperCase(res);
    }

    #passwordGenerator (name) {
        if(name.length > 5) {
            return this.#removeSpaces(name);
        } else {
            return "Minimal karakter yang diinputkan adalah 5 karakter";
        }
    }
}

const user1 = new User({
    name: 'Hafid',
    email: 'hafid@mail.com',
    password: 'pass123',
});

console.log(user1);
// User { name: 'Hafid', email: 'hafid@mail.com', password: '321SSBP' }