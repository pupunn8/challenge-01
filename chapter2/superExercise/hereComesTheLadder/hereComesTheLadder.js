let kata = 'foxie';

const hereComesTheLadder = (kata) => {
    let res = "";
    for (let i = kata.length - 1; i >= 0; i--) {
        for (let j = i; j > 0; j--) {
            res += " ";
        }
        if(i % 2 == 1) {
            for (let j = kata.length-1; j >= i; j--) {
                res += kata[j];
            }
        } else {
            for (let j = i; j < kata.length; j++) {
                res += kata[j];
            }
        }
        res += "\n";
    }

    return res;
};

console.log(hereComesTheLadder(kata));