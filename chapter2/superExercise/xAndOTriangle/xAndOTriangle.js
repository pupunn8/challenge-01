let num = 6;

const xAndOTriangle = num => {
    let res = "";
    for (let i = 0; i < num; i++) {
        for (let j = num - 1; j > i; j--) {
            res += " ";
        }
        for (let k = 0; k <= i; k++) {
            if(k >= 1) res += "o";
            res += "x";
        }
        res += "\n";
    }

    return res;
}

console.log(xAndOTriangle(num));