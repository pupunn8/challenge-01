let num = 6;

const shapingLetter = num => {
    let result = "";
    if( num >= 4 && num <= 10) {
        for (let i = 0; i < num; i++) {
            result += "#"
        }
        let y = num - 2;
        if(num % 2 == 0) {
            for (let j = 0; j < y; j++) {
                result += "\n ||"
            }
        } else {
            for (let j = 0; j < y; j++) {
                result += "\n |"
            }
        }
        result += "\n";
        for (let i = 0; i < num; i++) {
            result += "#"
        }
    } else {
        return "Invalid Number";
    }

    return result;
}

console.log(shapingLetter(num));