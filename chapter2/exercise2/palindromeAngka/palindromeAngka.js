let angka = 4364;

const palindrome = angka => {

    let number      = parseInt(angka);
    let condition   = true;

    if (number < 0 || isNaN(number)) return "Invalid input"
    if (number < 10) number = 10;

    while (condition) {
        let angka1  = "";
        let angka2  = "";
        let str     = number.toString();

        for (let i = 0; i < str.length; i++) {
            angka1 += str[i];
        }
        for (let j = str.length - 1; j >= 0; j--) {
            angka2 += str[j];
        }

        if(angka1 === angka2) {
            condition = false;
        } else {
            number++;
        }
    }

    return number;
};

console.log(palindrome(angka));