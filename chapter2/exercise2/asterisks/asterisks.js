console.log("\n Hasil soal pertama : \n");

let rows1 = 5; // input the number of rows

const barisanBintang = (rows1) => {
    for (let i = 0; i < rows1; i++) {
        console.log("*");
    }
};

barisanBintang(rows1);

console.log("\n Hasil soal kedua : \n");

let rows2 = 5;

const barisanBintangNested = (rows2) => {
    for (let i = 0; i < rows2; i++) {
        let res = "";
        for (let j = 0; j < rows2; j++) {
            res += "*";
        }
        console.log(res);
    }
}

barisanBintangNested(rows2);

console.log("\n Hasil soal ketiga : \n");

let rows3 = 5;

const barisanTanggaBintang = (rows3) => {
    for (let i = 0; i < rows3; i++) {
        let res = "";
        for (let j = 0; j <= i; j++) {
            res += "*";
        }
        console.log(res);
    }
}

barisanTanggaBintang(rows3);

console.log("\n Hasil soal keempat : \n");

let rows4 = 5;

const barisanTanggaBintangTerbalik = (rows4) => {
    for (let i = 0; i < rows3; i++) {
        let res = "";
        for (let j = 5; j > i; j--) {
            res += "*";
        }
        console.log(res);
    }
}

barisanTanggaBintangTerbalik(rows4);