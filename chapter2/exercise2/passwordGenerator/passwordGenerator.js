// Fungsi tambahan sendiri
function changeChar(arr1, arr2, str) {
    let res = "";
    for(let i = 0; i < str.length; i++) {
        let temp        = "";
        for (let j = 0; j < arr1.length; j++) {
            if(str[i] === arr1[j]) {
                temp = arr2[j];
            } else if(str[i] === arr2[j] && arr1.length == 26) {
                temp = arr1[j];
            }
        }
        
        if(temp) {
            res += temp;
        } else {
            res += str[i];
        }
    }

    return res;
};

function changeVocals(str) {
    // const arrKata1  = ["A", "E", "I", "U", "O", "a", "e", "i", "u", "o"];
    // const arrKata2  = ["B", "F", "J", "V", "P", "b", "f", "j", "v", "p"];
    const charCode  = [1, 5, 9, 15, 21];
    const arrKata1  = [];
    const arrKata2  = [];

    for (let i = 0; i < charCode.length; i++) {
        arrKata1[arrKata1.length] = String.fromCharCode(charCode[i] + 64);
        arrKata1[arrKata1.length] = String.fromCharCode(charCode[i] + 96);
        arrKata2[arrKata2.length] = String.fromCharCode(charCode[i] + 65);
        arrKata2[arrKata2.length] = String.fromCharCode(charCode[i] + 97);
    }

    return changeChar(arrKata1, arrKata2, str);
}
  
// console.log(changeVocals('Sergei Dragunov')) // Sfrgfj Drbgvnpv
// console.log(changeVocals('Irsyah Mardiah')) // Jrsybh Mbrdjbh

function reverseWord(str) {
    let res = "";
    for (let i = str.length - 1; i >= 0; i--) {
        res += str[i];
    }

    return changeVocals(res);
}
  
// console.log(reverseWord('I Love JavaScript')) // tpircSavaJ evoL I
// console.log(reverseWord('Irsyah Mardiah')) // haidraM haysrI

function setLowerUpperCase(str) {
    const kecil     = [];
    const besar     = [];
    const length    = 26;

    for (let i = 0; i < length; i++) {
        kecil[kecil.length] = String.fromCharCode(i + 65);
        besar[besar.length] = String.fromCharCode(i + 97);
    }

    let res = changeChar(kecil, besar, str);

    return reverseWord(res);
}
  
// console.log(setLowerUpperCase('i love hacktiv8')) // I LOVE HACKTIV8
// console.log(setLowerUpperCase('Irsyah Mardiah')) // iRSYAH mARDIAH

function removeSpaces(str) {
    let res = "";
    for (let i = 0; i < str.length; i++) {
        if (str[i] !== " ") {
            res += str[i];
        }
    }
    return setLowerUpperCase(res);
}

// console.log(removeSpaces('Sergei Dragunov')) // SergeiDragunov
// console.log(removeSpaces('Irsyah Mardiah')) // IrsyahMardiah

function passwordGenerator (name) {
    if(name.length > 5) {
        return removeSpaces(name);
    } else {
        return "Minimal karakter yang diinputkan adalah 5 karakter";
    }
}

console.log(passwordGenerator('Sergei Dragunov')); // 'VPNVGBRdJFGRFs'
console.log(passwordGenerator('Dimitri Wahyudiputra')); // 'BRTVPJDVYHBwJRTJMJd'
console.log(passwordGenerator('Alexei')); // 'JFXFLb'
console.log(passwordGenerator('Alex')); // 'Minimal karakter yang diinputkan adalah 5 karakter'