/**
  ============
  Capital case
  ============
 
  Sebuah fungsi bernama capitalCase akan menerima satu buah parameter yang berupa array of string,
  Fungsi ini akan mengembalikan array of string yang dimana setiap huruf pertama pada element nya
  akan berubah menjadi huruf besar
 
  Example:
  @input -> ['semmi', 'verian', 'putera']
  @output -> ['Semmi', 'Verian', 'Putera']
 
 */

 function capitalCase(arr) {
    // Insert your code here

    // langsung kembalikan .map (ingat bahwa .map mengembalikan array baru)
    return arr.map((eachElementOfArr) => {

      // Pertama kita spread dia, agar dari ['semmi'] berubah menjadi ['s', 'e', 'm', 'm', 'i'] dan kita bisa mengakses maupun mengubah elemen2nya.
      eachElementOfArr = [...eachElementOfArr];
      
      // Kedua kita ubah elemen paling depannya, yaitu index ke-0 menjadi Upper Case.
      eachElementOfArr[0] = eachElementOfArr[0].toUpperCase();
      
      // Kita satukan si array menggunakan .join (ingat bahwa .join mengembalikan array baru) lalu kembalikan;
      return eachElementOfArr.join('');
      
    })
}

console.log(capitalCase(['semmi', 'verian', 'putera', 'sOdQo'])) // ['Semmi', 'Verian', 'Putera']
console.log(capitalCase(['Naufal', 'muhaMmad', 'siddiq', 'lmkUi'])) // ['Naufal', 'MuhaMmad', 'Siddiq']
