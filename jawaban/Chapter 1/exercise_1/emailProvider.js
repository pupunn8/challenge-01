/**
  ==========================
  What is my email provider?
  ==========================
 
  Description
  -----------
  Email merupakan sebuah cara untuk kita berinteraksi antar satu dengan yang lainnya secara elektronik,
  Banyak sekali provider yang menyediakan layanan email ini.
 
  Instruction
  -----------
  Buatlah sebuah FUNGSI yang akan mengeluarkan output provider email yang digunakan oleh user.
 
  Contoh
  =======
  @input => lingga@gmail.com
  @output => Your email provider is gmail
  (tidak menggunakan .com di belakang)
  
  @input => angga@kampusmerdeka.gov.id
  @output => Your email provider is kampusmerdeka
  (tidak menggunakan .gov.id di belakang)
 
  Rules
  =====
  1. Tidak diperbolehkan menggunakan built-in function:
     .map .filter .reduce .split .join .indexOf .findIndex .substring
 
 */

// your code here
const emailProvider = (emailAddress) => {
  let result = ''; // Variabel ini kita buat untuk menampung hasil.
  let condition = false; // Variabel ini kita buat sebagai 'flagging' kondisi yang kita inginkan.
  let counter = 0; // Variabel ini kita buat untuk menargetkan jumlah dot ATAU '.' yang kita inginkan.

  [...emailAddress].forEach((element) => {
    
    if (element === '.') counter++; // Jika element merupakan '.' atau dot maka increment si counter;
    
    // Kita set kondisi ke 'true' karena looping forEach telah menemukan si simbol '@'.
    if (element === '@') condition = true; 
    // Kita melakukan pengecekan apabila si 'condition' sudah 'true' dan 'counter' masih bernilai 0, 
    // maka saatnya menambahkan tiap element ke variabel 'result'
    else if (condition && !(counter)) {
      result += element;
    } 
    
  });

  return result;
}

console.log(emailProvider('bahrudan@gmail.com'));
console.log(emailProvider('coriander@microsoft-azure.mail.com'));