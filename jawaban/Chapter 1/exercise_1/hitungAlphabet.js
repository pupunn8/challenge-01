/**
  ==========================
    HITUNG ALFABET LAGI?!?
  ==========================
  Function hitungAlfabet memiliki input sebuah kalimat (String).
  Tugasmu menghitung panjang kalimat tersebut dan menghitung huruf vokal dan huruf kosonan pada input.
  
  Huruf vokal: A I U E O. 
  Huruf kosonan : merupakan huruf alfabet tanpa huruf vokal.
  (huruf upper & lower case diperhitungkan)
  
  Output dari function ini merupakan object dengan key huruf dan panjang. 
  lihat pada contoh atau test case untuk lebih jelasnya.
  
 */


 function hitungAlfabet(str) {
    // your code here
    
    // Mari kita membuat satu variabel yang menampung 'object' yang akan kita kirim.
    const result = {
      huruf: {
        vokal: 0,
        konsonan: 0,
      },
      // Kita langsung set si panjang dengan 'length' si 'str'
      panjang: str.length,
    };
    
    // Lalu, kita jadikan parameter 'str' menjadi huruf kecil semua.
    let newStr = str.toLowerCase();

    // Mari membuat dua variabel lagi yang menampung semua huruf vokal (lowerCase) dalam array.
    const hurufVokal = [...'aiueo']; 
    const hurufKonsonan = [...'bcdfghjklmnpqrstvwxyz'];
    
    // Sekarang saatnya kita bermain dengan kondisi.
    [...newStr].forEach((tiapElementStr) => {
      // Kita gunakan built-in function 'includes' untuk melakukan pengecekan.

      // Kalau dia termasuk dalam array 'hurufVokal' maka increment si vokal.
      if (hurufVokal.includes(tiapElementStr)) result.huruf.vokal++;

      // Kalau dia termasuk dalam array 'hurufKonsonan' maka increment si konsonan.
      else if (hurufKonsonan.includes(tiapElementStr)) result.huruf.konsonan++;
    });
    return result;
}

console.log(hitungAlfabet('javascript!'))
/*
{
  huruf: {
    vokal: 3,
    konsonan: 7
  },
  panjang: 11
}
*/

console.log(hitungAlfabet('When I get older losing my hair'))
/*
{
  huruf: {
    vokal: 9,
    konsonan: 16
  },
  panjang: 31
}
*/

console.log(hitungAlfabet('2020 20 20 ## && +'))
  /*
{
  huruf: {
    vokal: 0,
    konsonan: 0
  },
  panjang: 18
}
*/