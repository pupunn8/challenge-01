/* 
Diberikan sebuah variabel `angka` bertipe number. 
Buatlah sebuah FUNGSI yang menerima satu buah PARAM bernama `angka` yang dapat menentukan pasangan dua digit angka mana yang paling besar. 

Contoh: let angka = 183928

maka output yang dihasilkan adalah 92.

penjelasan:

- 18 merupakan pasangan angka ke-1
- 83 merupakan pasangan angka ke-2
- 39 merupakan pasangan angka ke-3
- 92 merupakan pasangan angka ke-4
- 28 merupakan pasangan angka ke-5

Maka 92 adalah pasangan dua digit angka yang paling besar diantara yang lainnya.

Testlah program kamu dengan value angka dibawah ini:
    let angka = 641573  //73
    let angka = 12783456 //83
    let angka = 910233 //91
    let angka = 79918293 //99
*/

let angka = 79918293;

function pasanganAngka(angka) {
    // Kita buat array penampung yang memecah pasangan angka.
    const coupledArray = [];

    // Kita ubah angka ke string lalu spread si angka ke dalam array.
    // Gunakan .forEach untuk akses tiap elemen array.
    [...String(angka)].forEach((tiapElemenAngka, i, arr) => {
        // Perhatikan baik-baik disini, karena bentuk 'tiapElemenAngka' adalah string maka kita tambahkan dahulu elemennya agar menjadi angka yang berpasangan (tiapElemenAngka = arr[i + 1]).
        // Setelah itu kita gunakan pengubah tipe seperti 'parseInt' untuk mengubahnya langsung menjadi angka.
        // Lalu, kita gunakan .push untuk menambahkan angka ke dalam variabel 'coupledArray'
        if (i !== arr.length - 1) {
            coupledArray.push(parseInt(tiapElemenAngka + arr[i + 1]))
        }
    })
    return Math.max(...coupledArray);
}

console.log(pasanganAngka(angka));