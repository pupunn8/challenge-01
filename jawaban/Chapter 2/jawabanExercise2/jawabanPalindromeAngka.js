const palindromeAngka = (num) => {
    let result = 0;
    let condition = true;
    while (condition) {
        let strNum = String(num);
        let reverseStrNum = '';
        
        for (let j = strNum.length - 1; j >= 0; j--) {
            console.log(`nilai ${strNum[j]}, dengan nilai index j = ${j}`);
            reverseStrNum += strNum[j];
        }
        console.log(strNum, reverseStrNum);
        if (strNum === reverseStrNum && strNum.length !== 1) {
            result = Number(strNum);
            condition = false;
        }
        num++;
    }
    return result;
}
console.log(palindromeAngka(21));