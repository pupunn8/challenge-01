const mengelompokkanAngka = (arr) => {
    const arrResult = [[], [], []];
    let counterKelipatan2 = 0;
    let counterArrGanjil = 0;
    let counterKelipatan3 = 0;
    for (let j = 0; j < arr.length; j++) {
        if (!(arr[j] % 3)) {
            arrResult[2][counterKelipatan3] = arr[j];
            counterKelipatan3++;
        }
        else if (!(arr[j] % 2)) {
            arrResult[0][counterKelipatan2] = arr[j];
            counterKelipatan2++;
        } else  {
            arrResult[1][counterArrGanjil] = arr[j];
            counterArrGanjil++;
        }
    }
    return arrResult;
}

console.log(mengelompokkanAngka([2, 4, 6])) // [ [2, 4], [], [6] ]
console.log(mengelompokkanAngka([1, 2, 3, 4, 5, 6, 7, 8, 9])) // [ [ 2, 4, 8 ], [ 1, 5, 7 ], [ 3, 6, 9 ] ]
console.log(mengelompokkanAngka([100, 151, 122, 99, 111])) // [ [ 100, 122 ], [ 151 ], [ 99, 111 ] ]
console.log(mengelompokkanAngka([])) // [ [], [], [] ]