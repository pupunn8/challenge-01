const firstAsterisk = (row) => {
    // Inisialisasi looping dengan variabel 'i' SAMA DENGAN 0.
    // Lakukan looping dengan kondisi yaitu KURANG DARI 'row'.
    // Di setiap akhir looping tambah 'i' dengan 1 => i++ atau i += 1
    for (let i = 0; i < row; i++) {
        // Tampilkan si asterisk.
        console.log('*');
    }
    // Selesai.
}
firstAsterisk(5);

const secondAsterisks = (row) => {
    // Inisialisasi looping dengan variabel 'i' SAMA DENGAN 0.
    // Lakukan looping dengan kondisi yaitu KURANG DARI 'row'.
    // Di setiap akhir looping tambah 'i' dengan 1 => i++ atau i += 1
    for (let i = 0; i < row; i++) {
        // Inisialisasi dan assign 'word' dengan string kosong.
        let word = '';

        // Inisialisasi looping dengan variabel 'j' SAMA DENGAN 0.
        // Lakukan looping dengan kondisi yaitu KURANG DARI 'row'.
        // Di setiap akhir looping tambah 'i' dengan 1 => i++ atau i += 1
        
        // Tujuan looping kedua yaitu untuk concatenate / menambahkan si asterisk ke variabel 'word'.
        for (let j = 0; j < row; j++) {
            // Tambahkan asterisk ke variabel word, jangan lupa tambahkan spasi.
            word += '* '
        }
        // Tampilkan hasil tiap baris.
        console.log(word);
    }
    // Selesai.
}
secondAsterisks(5);

const thirdAsterisks = (row) => {
    // Inisialisasi looping dengan variabel 'i' SAMA DENGAN 0.
    // Lakukan looping dengan kondisi yaitu KURANG DARI 'row'.
    // Di setiap akhir looping tambah 'i' dengan 1 => i++ atau i += 1
    for (let i = 0; i < row; i++) {
        // Inisialisasi dan assign 'word' dengan string kosong.
        let str = '';

        // Inisialisasi looping dengan variabel 'j' SAMA DENGAN 0.
        // Lakukan looping dengan kondisi yaitu KURANG DARI 'i'.
        // Di setiap akhir looping tambah 'i' dengan 1 => i++ atau i += 1

        // Tujuan looping kedua adalah untuk concatenate / menambahkan si asterisk ke variabel 'word' TETAPI dengan memperhatikan nilai 'i'.
        for (let j = 0; j <= i; j++) {
            str += '* '
        }
        console.log(str);
    }
}
thirdAsterisks(5);

const fourthAsterisks = (row) => {
    // Inisialisasi looping dengan variabel 'i' SAMA DENGAN 0.
    // Lakukan looping dengan kondisi yaitu KURANG DARI 'row'.
    // Di setiap akhir looping tambah 'i' dengan 1 => i++ atau i += 1
    for (let i = 0; i < row; i++) {
        // Inisialisasi dan assign 'word' dengan string kosong.
        let str = '';

        // Inisialisasi looping dengan variabel 'j' SAMA DENGAN 'row'.
        // Lakukan looping dengan kondisi yaitu LEBIH DARI 'i'.
        // Di setiap akhir looping kurang 'j' dengan 1 => j-- atau j -= 1

        // Tujuan looping kedua adalah untuk concatenate / menambahkan si asterisk ke variabel 'word' TETAPI dengan memperhatikan nilai 'i' dan'row'.
        for (let j = row; j > i; j--) {
            str += '* '
        }
        console.log(str);
    }
}
fourthAsterisks(5);

