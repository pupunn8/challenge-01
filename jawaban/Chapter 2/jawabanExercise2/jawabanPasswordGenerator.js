function changeVocals (str) {
    //code di sini
    let vocal = 'aiueoAIUEO';
    let vocalPlusOne = 'bjvfpBJVFP';
    let result = '';

    for (let i = 0; i < str.length; i++) {
        let condition = false;
        for (let j = 0; j < vocal.length; j++) {
            if(str[i] === vocal[j])  {
                result += vocalPlusOne[j];
                j = vocal.length - 1;
                condition = true;
            }
        }
        if (!condition) {
            result += str[i];
        };
    }  
    return result;
}

function reverseWord (str) {
// code di sini
    let changedVocalPassword = changeVocals(str);
    let result = '';
    for (let i = changedVocalPassword.length - 1; i >= 0; i--) {
        result += changedVocalPassword[i]; 
    }
    return result;
}

function setLowerUpperCase (str) {
    //code di sini
    const reversedPassword = reverseWord(str);
    const lowerCase = 'abcdefghijklmnopqrstuvwxyz';
    const upperCase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < reversedPassword.length; i++) {
        let condition = false;
        for (let j = 0; j < lowerCase.length; j++) {
            if (reversedPassword[i] === lowerCase[j]) {
                result += upperCase[j];
                j = lowerCase.length - 1;
                condition = true;
            } else if (reversedPassword[i] === upperCase[j]) {
                result += lowerCase[j];
                j = lowerCase.length - 1;
                condition = true;
            }
        }
        if (!condition) {
            result += reversedPassword[i];
        }
    }
    return result;
}

function removeSpaces (str) {
    //code di sini
    const assignedCase = setLowerUpperCase(str);
    let result = '';
    
    for (let i = 0; i < assignedCase.length; i++) {
        if (!(assignedCase[i] === ' ')) result += assignedCase[i];
    }
    
    return result;
}

function passwordGenerator (name) {
//code di sini
    if (name.length < 5) return 'Minimal karakter yang diinputkan adalah 5 karakter';
    return removeSpaces(name);
}

console.log(passwordGenerator('Sergei Dragunov')); // 'VPNVGBRdJFGRFs'
console.log(passwordGenerator('Dimitri Wahyudiputra')); // 'BRTVPJDVYHBwJRTJMJd'
console.log(passwordGenerator('Alexei')); // 'JFXFLb'
console.log(passwordGenerator('Alex')); // 'Minimal karakter yang diinputkan adalah 5 karakter'